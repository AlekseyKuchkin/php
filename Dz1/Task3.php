<?php
/**
 * Created by PhpStorm.
 * User: aleksey
 * Date: 01.04.18
 * Time: 10:59
 */

$num=7;
function factorial($n){
    if ($n < 0) return 0;
    if ($n == 0) return 1;
    return $n*factorial($n-1);
}
echo ("Задача 3: ");
echo (factorial($num));
