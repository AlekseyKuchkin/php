<?php
class Calculator{
    private $firstNumber;
    private $secondNumber;

    public function __construct($firstNumber, $secondNumber)
    {
        $this->firstNumber = $firstNumber;
        $this->secondNumber = $secondNumber;
    }

    public function add(){
        return $this->firstNumber+$this->secondNumber;
    }

    public function multiply(){
        return $this->firstNumber*$this->secondNumber;
    }

    public function divide(){
        return $this->firstNumber/$this->secondNumber;
    }

    public function subtract(){
        return $this->firstNumber-$this->secondNumber;
    }
}
$myCalc = new Calculator(12,6);
echo $myCalc->add().'<br>';
echo $myCalc->multiply().'<br>';
echo $myCalc->divide().'<br>';
echo $myCalc->subtract().'<br>';
