<?php
class FormBuilder{

    public function open($arr){
        $formTag='<form';
        foreach ($arr as $key=>$value)
            $formTag.=(' '.$key.'='.'"'.$value.'"');
        $formTag.='>';
        return $formTag;
    }

    public function input($arr){
        $inputTag='<input';
        foreach ($arr as $key=>$value)
            $inputTag.=(' '.$key.'='.'"'.$value.'"');
        $inputTag.='>';
        return $inputTag;
    }

    public function submit($arr){
        $buttonTag='<button type="submit">';
        foreach ($arr as $value)
            $buttonTag.=$value;
        $buttonTag.='</button>';
        return $buttonTag;
    }

    public function close(){
        $formCloseTag='</form>';
        return $formCloseTag;
    }
}
