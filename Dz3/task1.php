<?php

class WorkerAlpha{
    public $name;
    public $age;
    public $salary;
}
$worker1=new WorkerAlpha();
$worker1->name="Ivan";
$worker1->age=25;
$worker1->salary=1000;

$worker2=new WorkerAlpha();
$worker2->name="Vasay";
$worker2->age=26;
$worker2->salary=2000;

echo 'Global age '.($worker1->age+$worker2->age).'<br>';
echo 'Global salary '.($worker1->salary+$worker2->salary).'<br>';
//----------------------------
class WorkerBeta{
    private $name;
    private $age;
    private $salary;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

}
$worker3=new WorkerBeta();
$worker3->setName("Ivan");
$worker3->setAge(25);
$worker3->setSalary(1000);

$worker4=new WorkerBeta();
$worker4->setName("Vasay");
$worker4->setAge(26);
$worker4->setSalary(2000);

echo 'Global age '.($worker3->getAge()+$worker4->getAge()).'<br>';
echo 'Global salary '.($worker3->getSalary()+$worker4->getSalary()).'<br>';
//----------------------------
class WorkerOmega{
    private $name;
    private $age;
    private $salary;
    const DEFAULT_AGE=18;

    private function checkAge($age){
        if ($age<100&&$age>1)
            return true;
        else
            return false;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAge($age)
    {
        if ($this->checkAge($age))
            $this->age = $age;
//        else $this->age = self::DEFAULT_AGE;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

}
$worker5=new WorkerOmega();
$worker5->setName("Ivan");
$worker5->setAge(25);
$worker5->setSalary(1000);

$worker6=new WorkerOmega();
$worker6->setName("Vasay");
$worker6->setAge(-10);
$worker6->setSalary(2000);

echo 'Global age '.($worker5->getAge()+$worker6->getAge()).'<br>';
echo 'Global salary '.($worker5->getSalary()+$worker6->getSalary()).'<br>';