<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Library</title>
    <link href="bootstrap.min.css" rel="stylesheet">
    <link href="starter-template.css" rel="stylesheet">
</head>
<body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="index.php">Top Library In The Galaxy</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="add.php">Add new book</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="show.php">Show the book</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="delete.php">Delete a book</a>
                </li>
            </ul>
        </div>
    </nav>
    <main role="main" class="container">
        <?php
        $i = strripos($_SERVER['REQUEST_URI'],'/');
        $site = substr($_SERVER['REQUEST_URI'],$i+1);
        if ($site=="index.php") {
            $db = mysqli_connect('127.0.0.1', 'root', '31415926', 'LibraryBooks');
            $books =$db->query("SELECT * FROM `Books`");
            $str = "";
            while ($row = $books->fetch_assoc())
            {
                $str.='<tr>
                        <td>'.$row["ID"].'</td>
                        <td>'.$row["Title"].'</td>
                        <td>'.$row["Author"].'</td>
                        <td>'.$row["Release_Date"].'</td>
                       </tr>';
            }
           echo '<div class="starter-template">
                    <h1>All books in the library</h1>
                 <table border="1" align="center" width="100%" cellpadding="10">
                     <tr>
                     <th>Book number</th>
                     <th>Book title</th>
                     <th>Book author</th>
                     <th>Release data</th>
                     </tr>
                     '.$str.'
                 </table>
                 </div>';
        }
        ?>
    </main>
</body>
</html>