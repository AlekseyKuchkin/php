<?php
include "index.php";
echo '<div class="starter-template">
        <h1>Add a book to the library</h1>
        <p>Fill in all the information about the book</p>
        <form class="my-2 my-lg-0" action="store.php" method="post" enctype="multipart/form-data">
            <label for="title">Book title:</label><br>
            <input required type="text" name="title"><br><br>
            <label for="date">Release data:</label><br>
            <input required type="date" name="date"><br><br>
            <label for="author">Book author:</label><br>
            <input required type="text" name="author"><br><br>
            <label for="file">Select a book:</label><br>
            <input required type="file" name="file"><br><br>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Add book</button>
        </form>
    </div>';