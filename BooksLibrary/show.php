<?php
include "index.php";
$id=$_POST['id'];
if ($id!=NULL)
{
    $db = mysqli_connect('127.0.0.1', 'root', '31415926', 'LibraryBooks');
    $book =$db->query("SELECT * FROM `Books` WHERE `ID`=$id");
    $book = $book->fetch_assoc();
    if ($book["Title"])
        echo '<div class="starter-template">
                <h1>'.$book["Title"].'</h1><br>
                <p class="lead">'.iconv('windows-1251', 'utf-8',file_get_contents($book["File_Path"])).'</p>
                </div>';
    else
    {
        echo '<div class="starter-template">
                    <h1>Book with requested id was not found</h1>
                    </div>';
    }

} else
    {
        echo '<div class="starter-template">
            <h1>Show the book from the library</h1>
            <p>Fill in the information about the book</p>
            <form class="my-2 my-lg-0" action="show.php" method="post" enctype="multipart/form-data">
                <label for="id">Book number in the library:</label><br>
                <input required type="text" name="id"><br><br>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Show</button>
            </form>
        </div>';
    }