-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 19 2018 г., 15:35
-- Версия сервера: 5.7.21-0ubuntu0.16.04.1
-- Версия PHP: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `LibraryBooks`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Books`
--

CREATE TABLE `Books` (
  `ID` int(11) NOT NULL,
  `Title` varchar(200) NOT NULL,
  `Release_Date` date NOT NULL,
  `Author` varchar(200) NOT NULL,
  `File_Path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `Books`
--

INSERT INTO `Books` (`ID`, `Title`, `Release_Date`, `Author`, `File_Path`) VALUES
(1, 'Ð”Ð¾Ð¼ ÑÑ‚Ñ€Ð°Ð½Ð½Ñ‹Ñ… Ð´ÐµÑ‚ÐµÐ¹', '2018-04-19', 'Ð ÐµÐ½ÑÐ¾Ð¼ Ð Ð¸Ð³Ð³Ð·', 'books/24756666.txt'),
(2, 'Ð¡Ñ‚Ñ€ÐµÐ»Ð¾Ðº', '2018-04-19', 'Ð¡Ñ‚Ð¸Ð²ÐµÐ½ ÐšÐ¸Ð½Ð³', 'books/29170436.txt'),
(3, 'ÐžÐ½Ð¾', '2018-04-19', 'Ð¡Ñ‚Ð¸Ð²ÐµÐ½ ÐšÐ¸Ð½Ð³', 'books/31998126.txt');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Books`
--
ALTER TABLE `Books`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Books`
--
ALTER TABLE `Books`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
