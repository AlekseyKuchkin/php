<?php
include "index.php";
$id=$_POST['id'];
if ($id!=NULL)
{
    $db = mysqli_connect('127.0.0.1', 'root', '31415926', 'LibraryBooks');
    $book =$db->query("SELECT * FROM `Books` WHERE `ID`=$id");
    $book = $book->fetch_assoc();
    $db->query("DELETE FROM `Books` WHERE `ID`=$id");
    unlink($book['File_Path']);
    if ($book["Title"])
        echo '<div class="starter-template">
                <h1>Book with title '.$book["Title"].' has been deleted</h1>
                </div>';
    else
        {
            echo '<div class="starter-template">
                    <h1>Book with requested id was not found</h1>
                    </div>';
        }

} else
    {
echo '<div class="starter-template">
        <h1>Delete a book from the library</h1>
        <p>Fill in the information about the book</p>
        <form class="my-2 my-lg-0" action="delete.php" method="post" enctype="multipart/form-data">
            <label for="id">Book number in the library:</label><br>
            <input required type="text" name="id"><br><br>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Delete</button>
        </form>
    </div>';
}